<?php
require('animal.php');
require('Ape.php');
require('Frog.php');

// $mini = new Mobil();
// $mini->jalan(); // menampilkan echo 'Mobil berjalan'
// echo $mini->roda; // 4

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>";// "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"


$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br><br>";
$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
$sungokong->yell() // "Auooo"

?>